//
//  ViewController.swift
//  SonarCloudSampleCheck
//
//  Created by Keshav MB on 25/04/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.sonarCloudSampleCheckProject()
    }

    func sonarCloudSampleCheckProject() {
        print("Sonar cloud sample project changes")
    }

}

